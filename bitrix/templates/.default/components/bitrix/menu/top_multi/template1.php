<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php d($arResult); ?>

<?php if ($arResult): ?>
    <div class="nv_topnav">
        <ul>
            <li><a href="/" class="menu-img-fon"
                   style="background-image: url(/bitrix/templates/.default/images/nv_home.png);"><span></span></a></li>

            <? foreach ($arResult as $arItem): ?>
                <? if ($arItem['DEPTH_LEVEL'] == 1): ?>
                    <li ><a href="<?= $arItem["LINK"] ?>"><span><?= $arItem["TEXT"] ?></span></a>
                    <?php if ($arItem["IS_PARENT"]): ?>
                        <ul>

                            <? foreach ($arResult as $arElem): ?>
                                <?php if ($arElem["CHAIN"][0] == $arItem["TEXT"]): ?>
                                    <li><a href="<?= $arElem['LINK'] ?>"><?= $arElem['TEXT'] ?></a></li>
                                <?php endif; ?>
                            <? endforeach; ?>

                        </ul>
                    <?php endif; ?>

                <? endif; ?>
                </li>
            <? endforeach; ?>

            <div class="clearboth"></div>
        </ul>
    </div>
<?php endif; ?>

