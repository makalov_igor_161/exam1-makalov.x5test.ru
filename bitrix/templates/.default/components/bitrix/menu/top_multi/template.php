<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?php if ($arResult): ?>
    <div class="nv_topnav">
        <ul>
                <li><a href="/" class="menu-img-fon"
                       style="background-image: url(/bitrix/templates/.default/images/nv_home.png);"><span></span></a>
                </li>
            <? foreach ($arResult as $arItem): ?>
                <? if ($arItem['DEPTH_LEVEL'] == 1 && $arItem["IS_PARENT"]): ?>
                    <li><a href="<?= $arItem["LINK"] ?>"><span><?= $arItem["TEXT"] ?></span></a>
                        <ul>
                            <? foreach ($arResult as $arElem): ?>
                                <?php if ($arElem['DEPTH_LEVEL'] > 1): ?>
                                    <li><a href="<?= $arElem['LINK'] ?>"><?= $arElem['TEXT'] ?></a></li>
                                <? endif; ?>
                            <? endforeach; ?>

                        </ul>
                    </li>
                <?php endif; ?>
                <? if ($arItem['DEPTH_LEVEL'] == 1 && $arItem["IS_PARENT"] == false): ?>
                    <li><a href="<?= $arItem["LINK"] ?>"><span><?= $arItem["TEXT"] ?></span></a></li>
                <?php endif; ?>

            <? endforeach; ?>

            <div class="clearboth"></div>
        </ul>
    </div>
<?php endif; ?>

