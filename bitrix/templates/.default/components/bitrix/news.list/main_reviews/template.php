<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php d($arResult);?>
.
        <div class="mn_content">
            <div class="main_post">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="review-block">
                    <div class="review-text">
                        <div class="review-block-title"><span class="review-block-name"><a href="#"><?=$arItem['NAME'];?></a></span><span class="review-block-description"><?= $arItem['PROPERTIES']['post']['VALUE']; ?></span></div>
                        <div class="review-text-cont">
                            <?= $arItem['PREVIEW_TEXT']; ?>
                        </div>
                    </div>
                    <div class="review-img-wrap"><img style="width: 65px; height: 65px;"src="<? if ($arItem['PREVIEW_PICTURE']['SRC']) {
                            echo $arItem['PREVIEW_PICTURE']['SRC'];
                        } else
                            echo "/upload/nophoto.jpg"; ?>" alt="img"></div>
                </div>
                <?endforeach;?>
                </div>
        </div>

