<?IncludeTemplateLangFile(__FILE__);?>
</div>
</div>
<div class="sb_sidebar">
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"left_menu_template", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "left_menu_template",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "N"
	),
	false
);?><br>
    <div class="sb_event">
        <div class="sb_event_header"><h4>Ближайшие события</h4></div>
        <p><a href="">29 августа 2012, Москва</a></p>
        <p>Семинар производителей мебели России и СНГ, Обсуждение тенденций.</p>
    </div>
    <?$APPLICATION->IncludeComponent("bitrix:news", "random_promo1", Array(
        "COMPONENT_TEMPLATE" => "random_promo",
        "IBLOCK_TYPE" => "Content",	// Тип инфоблока
        "IBLOCK_ID" => "11",	// Инфоблок
        "NEWS_COUNT" => "1",	// Количество новостей на странице
        "USE_SEARCH" => "N",	// Разрешить поиск
        "USE_RSS" => "N",	// Разрешить RSS
        "USE_RATING" => "N",	// Разрешить голосование
        "USE_CATEGORIES" => "N",	// Выводить материалы по теме
        "USE_REVIEW" => "N",	// Разрешить отзывы
        "USE_FILTER" => "N",	// Показывать фильтр
        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        "SEF_MODE" => "N",	// Включить поддержку ЧПУ
        "AJAX_MODE" => "N",	// Включить режим AJAX
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
        "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
        "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
        "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела
        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
        "USE_SHARE" => "N",	// Отображать панель соц. закладок
        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "LIST_FIELD_CODE" => array(	// Поля
            0 => "",
            1 => "",
        ),
        "LIST_PROPERTY_CODE" => array(	// Свойства
            0 => "",
            1 => "",
        ),
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        "DISPLAY_NAME" => "Y",	// Выводить название элемента
        "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
        "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
        "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
        "DETAIL_SET_CANONICAL_URL" => "N",	// Устанавливать канонический URL
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "DETAIL_FIELD_CODE" => array(	// Поля
            0 => "",
            1 => "",
        ),
        "DETAIL_PROPERTY_CODE" => array(	// Свойства
            0 => "",
            1 => "",
        ),
        "DETAIL_DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
        "DETAIL_PAGER_TITLE" => "Страница",	// Название категорий
        "DETAIL_PAGER_TEMPLATE" => "",	// Название шаблона
        "DETAIL_PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
        "PAGER_TITLE" => "Новости",	// Название категорий
        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
        "SET_STATUS_404" => "N",	// Устанавливать статус 404
        "SHOW_404" => "N",	// Показ специальной страницы
        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
        "VARIABLE_ALIASES" => array(
            "SECTION_ID" => "SECTION_ID",
            "ELEMENT_ID" => "ELEMENT_ID",
        )
    ),
        false
    );?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "random_review",
        Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "COMPONENT_TEMPLATE" => ".default",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(0=>"",1=>"",),
            "FILTER_NAME" => "",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "10",
            "IBLOCK_TYPE" => "Content",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "1",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array(0=>"post",1=>"",),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "STRICT_SECTION_CHECK" => "N"
        )
    );?>
</div>
<div class="clearboth"></div>
</div>
</div>

<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/include/footer.php');?>
</div>
</body>
</html>