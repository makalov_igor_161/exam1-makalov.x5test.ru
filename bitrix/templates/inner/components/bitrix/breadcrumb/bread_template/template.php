<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<div class="bc_breadcrumbs"><ul>';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++) {
    $title = $arResult[$index]["TITLE"];
    $link = $arResult[$index]["LINK"];
    $strReturn .= '<li><a href="'.$link.'">'. $title .'</a></li>';
}
$strReturn .= '</ul></div><div class="clearboth"></div>';

return $strReturn;
