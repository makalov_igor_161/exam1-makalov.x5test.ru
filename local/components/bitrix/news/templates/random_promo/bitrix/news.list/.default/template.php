<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
d($arResult);
?>

<div class="sb_action">
    <a href="">
        <?if($arResult['ITEMS']['0']['PREVIEW_PICTURE']):?>
            <img src="<?=$arResult['ITEMS']['0']['PREVIEW_PICTURE']['SRC'];?>" alt="<?=$arResult['PREVIEW_PICTURE']['ALT'];?>"/><?endif;?></a>
    <h4>Акция</h4>
    <h5><a href=""><?=$arResult['ITEMS']['0']['NAME'];?></a></h5>
    <a href="" class="sb_action_more">Подробнее &rarr;</a>
</div>

