<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if($arResult['ITEMS']):?>
    <?php foreach ($arResult['ITEMS'] as $arItem):?>
        <div class="ps_head">

            <a class="ps_head_link" href="<?=$arItem['DETAIL_PAGE_URL'];?>">

                <h2 class="ps_head_h"><?=$arItem['NAME'];?></h2></a>

            <span class="ps_date">Пройдет  <?=$arItem['PROPERTIES']['DATE_EVENT']['VALUE'];?>, <?=$arItem['PROPERTIES']['EVENT_PLACE']['VALUE'];?></span></div>

        <div class="ps_content">
            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" align="left" alt="<?=$arItem['PREVIEW_PICTURE']['ALT'];?>"/>
            <p><?=$arItem['DETAIL_TEXT'];?></p>

        </div>
    <?endforeach;?>
<?php endif;?>

